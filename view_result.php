<?php
session_start();
error_reporting(0);
include('includes/config.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Result Management System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

          <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar.php');?> 
                     <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->


                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-12">
                                    <!-- <h2 class="title" align="center">Result Management System</h2> -->
                                </div>
                            </div>
                            <!-- /.row -->
                          
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section" id="exampl">
                            <div class="container-fluid">

                                <div class="row">
                              
                             

                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h3 align="center">Student Result Details</h3>
                                                    <hr />
<?php
// code Student Data
 
$rollid=$_SESSION['rollid'];
$classid=$_SESSION['classid'];
$qery = "SELECT   tblstudents.StudentName,tblstudents.RollId,tblstudents.RegDate,tblstudents.StudentId,tblstudents.Status,tblclasses.ClassName,tblclasses.Section from tblstudents join tblclasses on tblclasses.id=tblstudents.ClassId where tblstudents.RollId=:rollid and tblstudents.ClassId=:classid ";
$stmt = $dbh->prepare($qery);
$stmt->bindParam(':rollid',$rollid,PDO::PARAM_STR);
$stmt->bindParam(':classid',$classid,PDO::PARAM_STR);
$stmt->execute();
$resultss=$stmt->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($stmt->rowCount() > 0)
{
foreach($resultss as $row)
{   ?>
<p><b>Student Name :</b> <?php echo htmlentities($row->StudentName);?></p>
<p><b>Student Roll Id :</b> <?php echo htmlentities($row->RollId);?>
<p><b>Student Class:</b> <?php echo htmlentities($row->ClassName);?>(<?php echo htmlentities($row->Section);?>)
<?php }

    ?>
                                            </div>
                                            <div class="panel-body p-20">







                                                <table id="example" class="table table-hover table-bordered" border="1" width="100%">
                                                  
                                                <thead>
                                                        <tr style="text-align: center">
                                                            <th style="text-align: center">#</th>
                                                            <th style="text-align: center"> Subject</th>    
                                                            <th style="text-align: center">Grade</th>
                                                            <th style="text-align: center">Marks</th>
                                                        </tr>
                                               </thead> 
                                               <tfoot id="tfooter" style="display: none;">
                                                        <tr style="text-align: center"> 
                                                        <?php 
                                                        
                                                        $cnt=1;
                                                        if($stmt->rowCount() > 0)
                                                        {
                                                        foreach($resultss as $row)
                                                        {   ?>
                                                        <th><p><b>Student Name :</b><br> <?php echo htmlentities($row->StudentName);?></p></th>
                                                        <th><p><b>Student Roll Id :</b><br> <?php echo htmlentities($row->RollId);?></th>
                                                        <th><p><b>Student Class:</b><br> <?php echo htmlentities($row->ClassName);?>(<?php echo htmlentities($row->Section);?>)</th>
                                                        <?php }}
                                                        ?>
                                                          
                                                        </tr>
                                               </tfoot>
  


                                                	
                                                	<tbody>
<?php                                              
// Code for result

 $query ="select t.StudentName,t.RollId,t.ClassId,t.marks,SubjectId,tblsubjects.SubjectName from (select sts.StudentName,sts.RollId,sts.ClassId,tr.marks,SubjectId from tblstudents as sts join  tblresult as tr on tr.StudentId=sts.StudentId) as t join tblsubjects on tblsubjects.id=t.SubjectId where (t.RollId=:rollid and t.ClassId=:classid)";
$query= $dbh -> prepare($query);
$query->bindParam(':rollid',$rollid,PDO::PARAM_STR);
$query->bindParam(':classid',$classid,PDO::PARAM_STR);
$query-> execute();  
$results = $query -> fetchAll(PDO::FETCH_OBJ);
$cnt=1;
$GPA = 0;
$GradeValue = 0;
$creditValue = 3;
$subjectVal = 0;
$subjectValTotal = 0;
if($countrow=$query->rowCount()>0)
{ 
foreach($results as $result){

    ?>

                                                		<tr>
<th scope="row" style="text-align: center"><?php echo htmlentities($cnt);?></th>
<td style="text-align: center"><?php echo htmlentities($result->SubjectName);?></td>
<td style="text-align: center">
    <?php if($result->marks >= 85){ $GradeValue = 4;?>A+
        <?php }else if($result->marks>=75 && $result->marks<=84){ $GradeValue = 4; ?>A
        <?php }else if($result->marks>=70 && $result->marks<=74){ $GradeValue = 3.7; ?>A-
        <?php }else if($result->marks>=65 && $result->marks<=69){ $GradeValue = 3.3; ?>B+ 
        <?php }else if($result->marks>=60 && $result->marks<=64){ $GradeValue = 3; ?>B
        <?php }else if($result->marks>=55 && $result->marks<=59){ $GradeValue = 2.7; ?>B-
        <?php }else if($result->marks>=50 && $result->marks<=54){ $GradeValue = 2.3; ?>C+
        <?php }else if($result->marks>=40 && $result->marks<=49){ $GradeValue = 2; ?>C
        <?php }else if($result->marks>=35 && $result->marks<=39){ $GradeValue = 1.7; ?>C-
        <?php }else if($result->marks>=31 && $result->marks<=35){ $GradeValue = 1.3; ?>D+
        <?php }else if($result->marks>=26 && $result->marks<=30){ $GradeValue = 1; ?>D
        <?php }else if($result->marks>=00 && $result->marks<=25){ $GradeValue = 0.7; ?>D-
        <?php }else { $GradeValue = 0.0; ?>E
        <?php } ?>  
        
        
        
 

        
        </td>
<td style="text-align: center"><?php echo htmlentities($totalmarks=$result->marks);?></td>
                                                		</tr>
<?php 
$subjectVal = $creditValue*$GradeValue;
$subjectValTotal = $subjectValTotal+$subjectVal;
$totlcount+=$totalmarks;
$cnt++;}

$GPA = $subjectValTotal/($creditValue*($cnt-1));

?>
<tr>
<th scope="row" colspan="2" style="text-align: center">Total Marks</th>
<td></td>
<td style="text-align: center"><b><?php echo htmlentities($totlcount); ?></b> out of <b><?php echo htmlentities($outof=($cnt-1)*100); ?></b></td>
                                                        </tr>
<tr>
<th scope="row" colspan="2" style="text-align: center">Percentage</th>
<td></td>           
<td style="text-align: center"><b><?php echo  htmlentities($totlcount*(100)/$outof); ?> %</b></td>
</tr>

<tr>
<th scope="row" colspan="2" style="text-align: center">GPA</th>
<td></td>           
<td style="text-align: center"><b><?php echo  $GPA; ?> </b></td>
</tr>

<tr>
                              
<!-- <td colspan="3" align="center"><i class="fa fa-print fa-2x" aria-hidden="true" style="cursor:pointer" OnClick="CallPrint(this.value)" ></i></td> -->
                                                             </tr>

 <?php } else { ?>     
<div class="alert alert-warning left-icon-alert" role="alert">
                                            <strong>Notice!</strong> Your result not declare yet
 <?php }
?>
                                        </div>
 <?php 
 } else
 {?>

<div class="alert alert-danger left-icon-alert" role="alert">
strong>Oh snap!</strong>
<?php
echo htmlentities("Invalid Roll Id");
 }
?>
                                        </div>



                                                	</tbody>
                                                </table>
                                                <input type="button" id="btnExport" value="Export" onclick="Export()" />

                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                   

                                 <!-- /.row -->
  
                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                  
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        function Export() {
            $('#tfooter').show();
            html2canvas(document.getElementById('example'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("results.pdf");
                    $('#tfooter').hide();
                }
            });
        }
    </script>
        
        <script>
            $(function($) {

            });


            function CallPrint(strid) {
var prtContent = document.getElementById("exampl");
var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();
}
</script>

        </script>

        

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->

    </body>
</html>
