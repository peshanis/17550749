<?php
session_start();
error_reporting(0);
include('includes/config.php');

 
if(isset($_POST['submit'])){
    // echo '<script>alert("Something Went Wrong. Please try again")</script>';
    
    $date1 = date("Y-m-d"); 

    $roleID = $_POST['roleID'];
    $subjectID = $_POST['subjectID'];
    $uploadCount = $_POST['uploadCount']; 
     
    //$time = date("h:i:s");

    $path = "assignment_upload/$subjectID/$roleID/";

    $files = glob($path.'/*'); // get all file names
            foreach($files as $file){ // iterate files
              if(is_file($file)) {
                unlink($file); // delete file
              }
            }
      
            if (!file_exists($path)) {
              mkdir($path, 0777, true);
          }

    for($i=1; $i <= $uploadCount; $i++){

        if(isset($_FILES['upload_'.$i])){
        
       
            $errors= array();
            $file_name = $_FILES['upload_'.$i]['name']; 
            $file_size =$_FILES['upload_'.$i]['size'];
            $file_tmp =$_FILES['upload_'.$i]['tmp_name'];
            $file_type=$_FILES['upload_'.$i]['type'];
            $file_ext=strtolower(end(explode('.',$_FILES['upload_'.$i]['name'])));
            // echo '<script>alert("Test Description has been updated. "'.$file_tmp.')</script>';
           
              
      
            $file_name = $subjectID."_".$roleID."_".$date1."_".$file_name;
       
            
            if(empty($errors)==true){ 
              move_uploaded_file($file_tmp,"$path".$file_name); 
               echo "Success"; 
         
             }
             else
               {
                 echo '<script>alert("Something Went Wrong. Please try again")</script>';
               }
        
        
            }else{
               print_r($errors);
            }
 
    }
  
    
   }

if (isset($_POST['submit']) && $_POST['StudentsAttempt']==0) {
    
    for($i=1; $i <= $uploadCount; $i++){
        $resourceURL = $resourceURL .'#'. $_FILES['upload_'.$i]['name'];
    } 
    // $resourceURL_1 = $_FILES['upload_'.$i]['name']; 
    // $resourceURL = $resourceURL_1 . '#' . $resourceURL_2. '#' . $resourceURL_3. '#' . $resourceURL_4. '#' . $resourceURL_5;



    $attemptCount = $_POST['attemptCount'];
     
    $sql = "INSERT INTO  tblassignment(roleID,subjectID,resourceURL,attemptCount) VALUES(:roleID,:subjectID,:resourceURL,:attemptCount)";
    $query = $dbh->prepare($sql);
    $query->bindParam(':roleID', $roleID, PDO::PARAM_STR);
    $query->bindParam(':subjectID', $subjectID, PDO::PARAM_STR);
    $query->bindParam(':resourceURL', $resourceURL, PDO::PARAM_STR);
    $query->bindParam(':attemptCount', $attemptCount, PDO::PARAM_STR);
    $query->execute();
    $lastInsertId = $dbh->lastInsertId();
    if ($lastInsertId) {
        $msg = "Assignment Uploaded successfully ";
    } else {
        $error = "Something went wrong. Please try again";
    }
}else if($_POST['StudentsAttempt']>0){
    
    for($i=1; $i <= $uploadCount; $i++){
        $resourceURL = $resourceURL .'#'. $_FILES['upload_'.$i]['name'];
    }
    $attemptCount = $_POST['attemptCount'];
    $sql="update  tblassignment set resourceURL=:resourceURL,attemptCount=:attemptCount where roleID=:roleID AND subjectID=:subjectID";
    $query = $dbh->prepare($sql);
    $query->bindParam(':roleID', $roleID, PDO::PARAM_STR);
    $query->bindParam(':subjectID', $subjectID, PDO::PARAM_STR);
    $query->bindParam(':resourceURL', $resourceURL, PDO::PARAM_STR);
    $query->bindParam(':attemptCount', $attemptCount, PDO::PARAM_STR);
    $query->execute();
    $msg = "Assignment Uploaded successfully";
}


// $info = pathinfo($_FILES[$resourceURL_1]['name']);
// $ext = $info['extension']; // get the extension of the file
// $newname = "newname.".$ext; 

// $target = 'images/'.$newname;
// move_uploaded_file( $_FILES[$resourceURL_1]['tmp_name'], $target);

// print_r($target);

 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment Submission</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="css/font-awesome.min.css" media="screen">
    <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen">
    <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen">
    <link rel="stylesheet" href="css/prism/prism.css" media="screen">
    <link rel="stylesheet" href="css/main.css" media="screen">
    <script src="js/modernizr/modernizr.min.js"></script>
</head>

<body class="top-navbar-fixed">
    <div class="main-wrapper">

        <!-- ========== TOP NAVBAR ========== -->
        <?php include('includes/topbar.php'); ?>
        <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
        <div class="content-wrapper">
            <div class="content-container">

                <!-- ========== TOP NAVBAR ========== -->
                <?php include('includes/topbar.php'); ?>
                <!-- ========== LEFT SIDEBAR ========== -->
                <?php include('includes/leftbar.php'); ?>
                <!-- /.left-sidebar -->


                <div class="main-page">
                    <div class="container-fluid">
                        <div class="row page-title-div">
                            <div class="col-md-12">
                                <!-- <h2 class="title" align="center">Result Management System</h2> -->
                            </div>
                        </div>
                        <!-- /.row -->

                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->

                    <section class="section" id="exampl">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h5>Assignment Submission</h5>
                                            </div>
                                        </div>
<?php
// code Student Data
 
$rollid=$_SESSION['rollid'];
$classid=$_SESSION['classid'];

?>

                                        <div class="panel-body">
                                            <?php if ($msg) { ?>
                                                <div class="alert alert-success left-icon-alert" role="alert">
                                                    <strong>Well done!</strong><?php echo htmlentities($msg); ?>
                                                </div><?php } else if ($error) { ?>
                                                <div class="alert alert-danger left-icon-alert" role="alert">
                                                    <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                                </div>
                                            <?php } ?>
                                            <form class="form-horizontal" method="post" enctype="multipart/form-data">




                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Module</label>
                                                    <div class="col-sm-10">
                                                        <select name="module" class="form-control" id="module" required="required" onchange="getValue()">
                                                            <option value="">Select Module</option>
                                                            <?php $sql = "select LecturerId,tblsubjects.SubjectName,lec.assignment_upload_count,assignment_attempt_count,assignment_deadline,t.SubjectId,attemptCount
                                                                            from (select sts.StudentName,sts.RollId,sts.ClassId,tr.marks,SubjectId 
                                                                            from tblstudents as sts join  tblresult as tr on tr.StudentId=sts.StudentId) as t join tblsubjects on tblsubjects.id=t.SubjectId 
                                                                            left join tbllecturers lec on tblsubjects.id = lec.ModuleId
                                                                            left join tblassignment assignment on lec.ModuleId = assignment.subjectID
                                                                            where (t.RollId=$rollid and t.ClassId=$classid);";
                                                            $query = $dbh->prepare($sql);
                                                            $query->execute();
                                                            $results = $query->fetchAll(PDO::FETCH_OBJ);
                                                            if ($query->rowCount() > 0) {
                                                                foreach ($results as $result) {   ?>
                                                                    <option value="<?php echo htmlentities($result->LecturerId); ?>#<?php echo htmlentities($result->assignment_upload_count); ?>#<?php echo htmlentities($result->assignment_attempt_count); ?>#<?php echo htmlentities($result->assignment_deadline); ?>#<?php echo htmlentities($result->SubjectId); ?>#<?php echo htmlentities($result->attemptCount); ?>"><?php echo htmlentities($result->SubjectName); ?></option>
                                                            <?php }?>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label"><span id="uploads" name="uploads"></span>  </label>
                                                    <div class="form-group col-sm-offset-2 col-sm-10" id="container"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="default" class="col-sm-4 control-label"><span id="attempts" name="attempts"></span>  </label>
                                                    <label for="default" class="col-sm-4 control-label"><span id="deadline" name="deadline"></span>  </label>
                                                     
                                                </div>

                                                
 



                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" name="submit" id="submit" class="btn btn-primary" onclick="saveDocuments();">Upload</button>
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                          <label for="default" class="col-sm-10 control-label"><span id="deadlineMessage" name="deadlineMessage"></span>  </label>
                                                      </div>
                                                </div>
                                                <input type="hidden" name="roleID" id="roleID" value="<?php echo $rollid ?>"/>
                                                <input type="hidden" name="subjectID" id="subjectID" />
                                                <input type="hidden" name="resourceURL" id="resourceURL" />
                                                <input type="hidden" name="attemptCount" id="attemptCount" /> 
                                                <input type="hidden" name="StudentsAttempt" id="StudentsAttempt" /> 
                                                <input type="hidden" name="uploadCount" id="uploadCount" /> 
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            document.addEventListener('contextmenu', event => event.preventDefault());
            $(function($) {

            });


            function CallPrint(strid) {
                var prtContent = document.getElementById("exampl");
                var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
            }
            function getValue(){
                var e = document.getElementById("module");//testCheck
                var module = e.value;

                var module = module.split('#');
                var lectureId = module[0];
                var uploadCount = module[1];
                var attemptCount = module[2];
                var deadline = module[3];
                var subjectID = module[4];
                var currentAttemptCount = module[5];

                
                if(currentAttemptCount==""){
                    currentAttemptCount=0;
                }

                document.getElementById("uploads").innerHTML = "You Can Upload "+uploadCount+" files.";
                var currentUploads = +attemptCount- +currentAttemptCount
                if((currentUploads) == 1 || (currentUploads) == 0 ){
                    document.getElementById("attempts").innerHTML = "<font color='orange'>You Have "+(attemptCount-currentAttemptCount)+" attempt counts.</font>";
                }else{
                    document.getElementById("attempts").innerHTML = "You Have "+(attemptCount-currentAttemptCount)+" attempt counts.";
                }
                

                document.getElementById("deadlineMessage").innerHTML = ""; 

                document.getElementById("subjectID").value = subjectID; 
                document.getElementById("attemptCount").value = +currentAttemptCount + 1;

                document.getElementById("StudentsAttempt").value = currentAttemptCount;
                document.getElementById("uploadCount").value = uploadCount;

                
                

                $('#submit').removeAttr('disabled');
                if(deadline!=""){
                    var date_deadLine = new Date(deadline)
                    var today = new Date(); 
                    document.getElementById("deadline").innerHTML = "Submission Deadline "+formatDate(date_deadLine)+" 23:59:59";
                     
                     

                const date1 = new Date(deadline);
                const date2 =  new Date();
                const diffTime = (date1 - date2);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));  

                if(diffDays<0){ 
                    $('#submit').attr('disabled','disabled');
                document.getElementById("deadlineMessage").innerHTML = "<br><font color='orange'>** Sorry, Submission date passed by "+-1*diffDays+" days. You cannot upload the Assignement. **</font>";
                    //document.getElementById("submit").disabled = true;
                }
                
                if((currentUploads)==0){ 
                    $('#submit').attr('disabled','disabled');
                document.getElementById("deadlineMessage").innerHTML = "<br><font color='orange'>** Sorry, All attempts are completed. **</font>";
                    //document.getElementById("submit").disabled = true;
                }
                    
                } 
                
                

                

                var number = uploadCount;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("container");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            for (i=1;i<=number;i++){
                // Append a node with a random text
                container.appendChild(document.createTextNode("File Upload " + (i)));
                // Create an <input> element, set its type and name attributes
                var input = document.createElement("input");
                input.type = "file";
                input.id = "upload_" + i;
                input.name = "upload_" + i;
                input.class = "form-control";
                input.accept = "application/pdf";
                container.appendChild(input);
                // Append a line break 
                container.appendChild(document.createElement("br"));
                
            }

            //document.getElementById("upload_1").value = outputfile.value;  
        }
           
        function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

function getFile(filePath) {
        return filePath.substr(filePath.lastIndexOf('\\') + 1).split('.')[0];
    }

    function saveDocuments(){
        //alert("hhhh");
    }
     
             
        </script> 

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->

</body>

</html>