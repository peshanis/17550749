<?php
session_start();
error_reporting(0);
include('includes/config.php');

 
if(isset($_POST['submit'])){
    // echo '<script>alert("Something Went Wrong. Please try again")</script>';
    
    $date1 = date("Y-m-d"); 

    $roleID = $_POST['roleID'];
    $subjectID = $_POST['subjectID'];
    $uploadCount = $_POST['uploadCount']; 
     
    //$time = date("h:i:s");

    $path = "assignment_upload/$subjectID/$roleID/";

    $files = glob($path.'/*'); // get all file names
            foreach($files as $file){ // iterate files
              if(is_file($file)) {
                unlink($file); // delete file
              }
            }
      
            if (!file_exists($path)) {
              mkdir($path, 0777, true);
          }

    for($i=1; $i <= $uploadCount; $i++){

        if(isset($_FILES['upload_'.$i])){
        
       
            $errors= array();
            $file_name = $_FILES['upload_'.$i]['name']; 
            $file_size =$_FILES['upload_'.$i]['size'];
            $file_tmp =$_FILES['upload_'.$i]['tmp_name'];
            $file_type=$_FILES['upload_'.$i]['type'];
            $file_ext=strtolower(end(explode('.',$_FILES['upload_'.$i]['name'])));
            // echo '<script>alert("Test Description has been updated. "'.$file_tmp.')</script>';
           
              
      
            $file_name = $subjectID."_".$roleID."_".$date1."_".$file_name;
       
            
            if(empty($errors)==true){ 
              move_uploaded_file($file_tmp,"$path".$file_name); 
               echo "Success"; 
         
             }
             else
               {
                 echo '<script>alert("Something Went Wrong. Please try again")</script>';
               }
        
        
            }else{
               print_r($errors);
            }
 
    }
  
    
   }

if (isset($_POST['submit']) && $_POST['StudentsAttempt']==0) {
    
    for($i=1; $i <= $uploadCount; $i++){
        $resourceURL = $resourceURL .'#'. $_FILES['upload_'.$i]['name'];
    } 
    // $resourceURL_1 = $_FILES['upload_'.$i]['name']; 
    // $resourceURL = $resourceURL_1 . '#' . $resourceURL_2. '#' . $resourceURL_3. '#' . $resourceURL_4. '#' . $resourceURL_5;



    $attemptCount = $_POST['attemptCount'];
     
    $sql = "INSERT INTO  tblassignment(roleID,subjectID,resourceURL,attemptCount) VALUES(:roleID,:subjectID,:resourceURL,:attemptCount)";
    $query = $dbh->prepare($sql);
    $query->bindParam(':roleID', $roleID, PDO::PARAM_STR);
    $query->bindParam(':subjectID', $subjectID, PDO::PARAM_STR);
    $query->bindParam(':resourceURL', $resourceURL, PDO::PARAM_STR);
    $query->bindParam(':attemptCount', $attemptCount, PDO::PARAM_STR);
    $query->execute();
    $lastInsertId = $dbh->lastInsertId();
    if ($lastInsertId) {
        $msg = "Assignment Uploaded successfully ";
    } else {
        $error = "Something went wrong. Please try again";
    }
}else if($_POST['StudentsAttempt']>0){
    
    for($i=1; $i <= $uploadCount; $i++){
        $resourceURL = $resourceURL .'#'. $_FILES['upload_'.$i]['name'];
    }
    $attemptCount = $_POST['attemptCount'];
    $sql="update  tblassignment set resourceURL=:resourceURL,attemptCount=:attemptCount where roleID=:roleID AND subjectID=:subjectID";
    $query = $dbh->prepare($sql);
    $query->bindParam(':roleID', $roleID, PDO::PARAM_STR);
    $query->bindParam(':subjectID', $subjectID, PDO::PARAM_STR);
    $query->bindParam(':resourceURL', $resourceURL, PDO::PARAM_STR);
    $query->bindParam(':attemptCount', $attemptCount, PDO::PARAM_STR);
    $query->execute();
    $msg = "Assignment Uploaded successfully";
}


// $info = pathinfo($_FILES[$resourceURL_1]['name']);
// $ext = $info['extension']; // get the extension of the file
// $newname = "newname.".$ext; 

// $target = 'images/'.$newname;
// move_uploaded_file( $_FILES[$resourceURL_1]['tmp_name'], $target);

// print_r($target);

 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment Submission</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="css/font-awesome.min.css" media="screen">
    <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen">
    <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen">
    <link rel="stylesheet" href="css/prism/prism.css" media="screen">
    <link rel="stylesheet" href="css/main.css" media="screen">
    <script src="js/modernizr/modernizr.min.js"></script>
    <style>
        .lect_list {
    columns: 2;
    -webkit-columns: 2;
    -moz-columns: 2;
}
    </style>
</head>

<body class="top-navbar-fixed">
    <div class="main-wrapper">

        <!-- ========== TOP NAVBAR ========== -->
        <?php include('includes/topbar.php'); ?>
        <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
        <div class="content-wrapper">
            <div class="content-container">

                <!-- ========== TOP NAVBAR ========== -->
                <?php include('includes/topbar.php'); ?>
                <!-- ========== LEFT SIDEBAR ========== -->
                <?php include('includes/leftbar.php'); ?>
                <!-- /.left-sidebar -->


                <div class="main-page">
                    <div class="container-fluid">
                        <div class="row page-title-div">
                            <div class="col-md-12">
                                <!-- <h2 class="title" align="center">Result Management System</h2> -->
                            </div>
                        </div>
                        <!-- /.row -->

                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->

                    <section class="section" id="exampl">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h5>Lecture Notes</h5>
                                            </div>
                                        </div>
<?php
// code Student Data
 
$rollid=$_SESSION['rollid'];
$classid=$_SESSION['classid'];

?>

                                        <div class="panel-body">
                                            <?php if ($msg) { ?>
                                                <div class="alert alert-success left-icon-alert" role="alert">
                                                    <strong>Well done!</strong><?php echo htmlentities($msg); ?>
                                                </div><?php } else if ($error) { ?>
                                                <div class="alert alert-danger left-icon-alert" role="alert">
                                                    <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                                </div>
                                            <?php } ?>
                                            <form class="form-horizontal" method="post" enctype="multipart/form-data">

  
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Module</label>
                                                    <div class="col-sm-10">
                                                        <select name="module" class="form-control" id="module" required="required" onchange="getValue()">
                                                            <option value="">Select Module</option>
                                                            <?php $sql = "select tblsubjects.SubjectCode,t.SubjectId,tblsubjects.SubjectName
                                                                            from (select sts.StudentName,sts.RollId,sts.ClassId,tr.marks,SubjectId 
                                                                            from tblstudents as sts join  tblresult as tr on tr.StudentId=sts.StudentId) as t join tblsubjects on tblsubjects.id=t.SubjectId 
                                                                            left join tbllecturers lec on tblsubjects.id = lec.ModuleId
                                                                            left join tblassignment assignment on lec.ModuleId = assignment.subjectID
                                                                            where (t.RollId=$rollid and t.ClassId=$classid);";
                                                            $query = $dbh->prepare($sql);
                                                            $query->execute();
                                                            $results = $query->fetchAll(PDO::FETCH_OBJ);
                                                            if ($query->rowCount() > 0) {
                                                                foreach ($results as $result) {   ?>
                                                                    <option value="<?php echo htmlentities($result->SubjectCode); ?>#<?php echo htmlentities($result->SubjectId); ?>"><?php echo htmlentities($result->SubjectName); ?></option>
                                                            <?php }?>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label"><span id="uploads" name="uploads"></span>  </label>
                                                    <div class="form-group col-sm-offset-2 col-sm-10" id="container"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="default" class="col-sm-4 control-label"><span id="attempts" name="attempts"></span>  </label>
                                                    <label for="default" class="col-sm-4 control-label"><span id="deadline" name="deadline"></span>  </label>
                                                     
                                                </div>

                                                <p id = "GFG_UP" style =
            "font-size: 19px; font-weight: bold;">
        </p>

        

                                                <p id = "GFG_DOWN" style =
            "color: green; font-size: 24px; font-weight: bold;">
        </p>
          
        <!-- <div id='fileNames'></div> -->

        <h5><u><div id="fileCount"></div></u></h5><br>
        <div id="files"></div>

    <div class="form-group">
                                                    <div class="col-sm-12">
                                                          <label for="default" class="col-sm-10 control-label"><span id="deadlineMessage" name="deadlineMessage"></span>  </label>
                                                      </div>
                                                </div>
                                                 
                                            </form>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script> 
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script>
            // document.addEventListener('contextmenu', event => event.preventDefault());
            $(function($) {

            });


            function CallPrint(strid) {
                var prtContent = document.getElementById("exampl");
                var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
            }
            function getValue(){ 
                var e = document.getElementById("module");//testCheck
                var module = e.value;

                var module = module.split('#');
                var subjectCode = module[0];
                var subjectID = module[1];  
                var fileList = "";
                $.ajax({
    url: "lecture_uploads/"+subjectCode+"/"+subjectID+"/",
    success: function(data) {
        var parser = new DOMParser(),
            doc = parser.parseFromString(data, 'text/html');

        //output the file table
        // $("#files").append(doc.querySelector('table').outerHTML);

        //or return the number of files
        //tr = icon, filename, date, size, desc
        //consider all rows with a size value holding a number as a vlid file
        var fileCount = 0,
            rows = doc.querySelector('table').querySelectorAll('tr');
             
        for (var i=0;i<rows.length;i++) {
            if (rows[i].children[3]) {
                if (parseInt(rows[i].children[3].innerText)>0) {
                // console.log(rows[i].children[1].innerHTML);

                // fileList = "lecture_uploads/"+subjectCode+"/"+subjectID+"/"+rows[i].children[1].innerHTML;

                var oldUrl = rows[i].children[1].innerHTML; // Get current url
                // console.log(oldUrl);
                
            var newUrl = oldUrl.replace("LEC_", "lecture_uploads/"+subjectCode+"/"+subjectID+"/LEC_"); // Create new url  
            $(this).attr("href", newUrl); // Set herf value

                // console.log(newUrl);
                

                $("#files").append("<ul><li>"+newUrl+"</li></ul>");

                

                fileCount++; 
                }        
            }
        }
        $("#fileCount").text(fileCount+" lecture notes were uploaded.");
    }
});


        //         $.get("lecture_uploads/MIT1204/5/", function(data) 
        // {
        //     $("#fileNames").append(data);
        // });

               // url: "lecture_uploads/MIT1204/5/",

// var assignmentList = "";

// var filenames=[], foldernames=[];

// $.get("lecture_uploads/"+subjectCode+"/"+subjectID+"/",function(response){
//     //document.write(response);
//     alert(response.files);

    
    

//     assignmentList = assignmentList +"<a href="+"lecture_uploads/"+subjectCode+"/"+subjectID+"/"+response+" >sssss</a><br>"    ;
//             var el_up = document.getElementById("GFG_UP");
            
              
//             // el_up.innerHTML = "  "+assignmentList;
//     getNames();
// });

// function getNames()
// {
//     var files = document.querySelectorAll("a.icon.file");
    
//     var folders = document.querySelectorAll("a.icon.dir");
//     files.forEach(function(item){filenames.push(item.textContent)})
//     folders.forEach(function(item){foldernames.push(item.textContent.slice(0,-1))})
//     console.log(filenames);
//     console.log(foldernames); 
// }




//                $.ajax({
//   url: "lecture_uploads/"+subjectCode+"/"+subjectID+"/",
//   success: function(data){
       
//      $(data).find("td > a").each(function(){
//         // will loop through 
//         console.log("Found a file: " + $(this).attr("href"));

//         assignmentList = assignmentList +"<a href='' >"+$(this).attr("href")+"</a><br>"    ;
 
//         var el_up = document.getElementById("GFG_UP");
            
              
//             el_up.innerHTML = "  "+assignmentList;
//      });
//   }
// });




                
//                 var filenames=[], foldernames=[];
// var url = "lecture_uploads/MIT1204/5/";
// var req = new XMLHttpRequest();
// req.open("GET",url,true);
// req.onreadystatechange=function(){
//     if(req.readyState === 4)
//     {
//        document.write(req.responseText);
//     //    console.log(req.responseText);
//         getNames();
//     }
// };
// req.send();

// function getNames()
// {
//     var files = document.querySelectorAll("a.icon.file");
//     var folders = document.querySelectorAll("a.icon.dir");
//     files.forEach(function(item){filenames.push(item.textContent)})
//     folders.forEach(function(item){foldernames.push(item.textContent.slice(0,-1))})
//     console.log(filenames);
//     console.log(foldernames);
// }
    
// var filenames=[], foldernames=[];

// $.get("srms/lecture_uploads/MIT1204/5/",function(response){
//     document.write(response);
//     getNames();
// });

// function getNames()
// {
//     var files = document.querySelectorAll("a.icon.file");
//     var folders = document.querySelectorAll("a.icon.dir");
//     files.forEach(function(item){filenames.push(item.textContent)})
//     folders.forEach(function(item){foldernames.push(item.textContent.slice(0,-1))})
//     console.log(filenames);
//     console.log(foldernames);
// }
     
               

            //document.getElementById("upload_1").value = outputfile.value;  
        }


        
              
            function GFG_Fun() {
                  
                // Create anchor element.
                var a = document.createElement('a'); 
                  
                // Create the text node for anchor element.
                var link = document.createTextNode("This is link");
                  
                // Append the text node to anchor element.
                a.appendChild(link); 
                  
                // Set the title.
                a.title = "This is Link"; 
                  
                // Set the href property.
                a.href = "https://www.geeksforgeeks.org"; 
                  
                // Append the anchor element to the body.
                document.body.appendChild(a); 
            }  
           
        function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

 
     
             
        </script> 

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->

</body>

</html>