<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

        



if(isset($_POST['Update']))
{

    if($_SESSION["aroleId"]==1){

$sid=intval($_GET['subjectid']);
$subjectname=$_POST['subjectname'];
$subjectcode=$_POST['subjectcode']; 


$sql="update  tblsubjects set SubjectName=:subjectname,SubjectCode=:subjectcode where id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':subjectname',$subjectname,PDO::PARAM_STR);
$query->bindParam(':subjectcode',$subjectcode,PDO::PARAM_STR);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$msg="Module Info updated successfully";

}else if($_SESSION["aroleId"]==2){
    $sid=intval($_GET['subjectid']);
    $subjectcode=$_POST['subjectcode'];
    $lec_id = $_SESSION["lLogID"]; 
    $Reg = "LEC";

    $path = "lecture_uploads/$subjectcode/$sid/";

          if (!file_exists($path)) {
              mkdir($path, 0777, true);
          }

    if (isset($_FILES['uploadLectNotes'])) {
        $myFile = $_FILES['uploadLectNotes'];
        $fileCount = count($myFile["name"]);
 
        for ($i = 0; $i < $fileCount; $i++) {
            $lectureURL = $lectureURL .'#'. $myFile["name"][$i];


            
       
                $errors= array();
                $file_name = $myFile["name"][$i];  
                $file_tmp = $myFile["tmp_name"][$i]; 
                // echo '<script>alert("Test Description has been updated. "'.$file_tmp.')</script>';
                $date1 = date("Y-m-d"); 
                  
          
                $file_name = $Reg."_".$subjectcode."_".$lec_id."_".$date1."_".$file_name;
           
                
                if(empty($errors)==true){ 
                  move_uploaded_file($file_tmp,"$path".$file_name); 
                   echo "Success"; 
             
                 }
                 else
                   {
                     echo '<script>alert("Something Went Wrong. Please try again")</script>';
                   }
            
            
                 



        }
    }
 
    
        
        $uploadCount=$_POST['uploadCount']; 
        $attemptCount=$_POST['attemptCount'];
        $assignmentDeadLine=$_POST['assignmentDeadLine'];
        $notifyChecker=$_POST['notifyChecker']; 
        $notification=$_POST['notification']; 
        $notificationTitle=$_POST['notificationTitle'];
        $currentDate = date('Y-m-d H:i:s');
        $sql="update  tbllecturers set assignment_upload_count= '$uploadCount' , lectureNotesURL = '$lectureURL' , assignment_attempt_count= '$attemptCount' , assignment_deadline = '$assignmentDeadLine' ,NotificationCheck = '$notifyChecker',NotificationTitle = '$notificationTitle',NotificationMessage = '$notification',UpdationDate = '$currentDate' where LecturerId = '$lec_id' ";

        $query = $dbh->prepare($sql);
        $query->execute();
        $msg="Module Info updated successfully";
    }


}
?>
 


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SMS Admin Update Subject </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Update Subject</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div"> 
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Subjects</li>
                                        <li class="active">Update Subject</li>
                                    </ul>
                                </div>
                             
                            </div> <br>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Update Subject</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                
                                               
                                        <?php if($_SESSION["aroleId"]==1){ ?>        
                                                
                                                <form class="form-horizontal" method="post">

 <?php
$sid=intval($_GET['subjectid']);
$sql = "SELECT * from tblsubjects where id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>                                               
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject Name</label>
                                                        <div class="col-sm-10">
 <input type="text" name="subjectname" value="<?php echo htmlentities($result->SubjectName);?>" class="form-control" id="default" placeholder="Subject Name" required="required">
                                                        </div>
                                                    </div>
<div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject Code</label>
                                                        <div class="col-sm-10">
 <input type="text" name="subjectcode" class="form-control" value="<?php echo htmlentities($result->SubjectCode);?>"  id="default" placeholder="Subject Code" required="required">
                                                        </div>
                                                    </div>
                                                    <?php }} ?>

                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" name="Update" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </form>



                                                <?php }else if($_SESSION["aroleId"]==2){ ?>

                                                    <form class="form-horizontal" method="post" enctype="multipart/form-data">

<?php
$sid=intval($_GET['subjectid']);
$lec_id = $_SESSION["lLogID"];
$sql = "SELECT * from tblsubjects left join tbllecturers on tblsubjects.id = tbllecturers.ModuleId  where id=:sid and tbllecturers.LecturerId = '+$lec_id+' ";
$query = $dbh->prepare($sql);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>                                               
                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label">Subject Name</label>
                                                       <div class="col-sm-10">
<input type="text" name="subjectname" value="<?php echo htmlentities($result->SubjectName);?>" class="form-control" id="default" placeholder="Subject Name" readonly>
                                                       </div>
                                                   </div>
<div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label">Subject Code</label>
                                                       <div class="col-sm-10">
<input type="text" name="subjectcode" class="form-control" value="<?php echo htmlentities($result->SubjectCode);?>"  id="default" placeholder="Subject Code" readonly>
                                                       </div>
                                                   </div>

                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label">Assignment Upload Count</label>
                                                       <div class="col-sm-3">
<input type="number" name="uploadCount" class="form-control" id="uploadCount" value="<?php echo htmlentities($result->assignment_upload_count);?>" placeholder="Assignment Upload Count" min="1" max="5"> 
                                                       </div>
                                                   </div>

                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label">Assignment Attempt Count</label>
                                                       <div class="col-sm-3">
<input type="number" name="attemptCount" class="form-control" id="attemptCount" value="<?php echo htmlentities($result->assignment_attempt_count);?>" placeholder="Assignment Attempt Count" >
                                                       </div>
                                                   </div>

                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label">Assignment Deadline</label>
                                                       <div class="col-sm-3">
<input type="date" name="assignmentDeadLine" class="form-control" id="assignmentDeadLine" value="<?php echo htmlentities($result->assignment_deadline);?>" placeholder="Assignment Resource Count">
                                                       </div>
                                                   </div>

                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label" id="lectureNotes" name="lectureNotes" >Upload Lecture Notes</label>
                                                       <div class="col-sm-4">
                                                       <input type="file"  name="uploadLectNotes[]" multiple class="form-control"  placeholder="Upload Lecture Notes">
                                                       </div>
                                                   </div>


                                                   <div class="form-group">
                                                   <label for="default" class="col-sm-2 control-label">Notify Students</label>
                                                   <div class="col-sm-2">
                                                   <input class="form-check-input" type="checkbox" value="0" id="notifyChecker" name="notifyChecker" onchange="showNotification()">
                                                   </div>
                                                   </div>

                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label" id="notificationTitleLabel" name="notificationTitleLabel" style="display:none">Notification Title</label>
                                                       <div class="col-sm-4">
                                                        <input type="text" id="notificationTitle" name="notificationTitle" style="display:none" class="form-control" value="<?php echo htmlentities($result->notificationTitle);?>" placeholder="notification title">
                                                         </div>
                                                   </div>

                                                   <div class="form-group">
                                                       <label for="default" class="col-sm-2 control-label" id="notificationLabel" name="notificationLabel" style="display:none">Notification Messege</label>
                                                       <div class="col-sm-4">
                                                       <textarea id="notification" name="notification" style="display:none" value="<?php echo htmlentities($result->NotificationMessage);?>" rows="4" cols="50" placeholder="small description about the notification"></textarea>
                                                       </div>
                                                   </div>
 

                                                   <?php }} ?>

                                                   
                                                   <div class="form-group">
                                                       <div class="col-sm-offset-2 col-sm-10">
                                                           <button type="submit" name="Update" class="btn btn-primary">Update</button>
                                                       </div>
                                                   </div>
                                               </form>

<?php }?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
            function showNotification(){ 
                
                    var notificationLabel = document.getElementById("notificationLabel");
                    var notification = document.getElementById("notification");

                    var notificationTitleLabel = document.getElementById("notificationTitleLabel");
                    var notificationTitle = document.getElementById("notificationTitle");

                    if (window.getComputedStyle(notificationLabel).display === "none") {
                        notificationLabel.style.display = "block";
                        notification.style.display = "block";

                        notificationTitleLabel.style.display = "block";
                        notificationTitle.style.display = "block";

                        document.getElementById("notifyChecker").value = 1;

                    }else{
                        notificationLabel.style.display = "none";
                        notification.style.display = "none";

                        notificationTitleLabel.style.display = "none";
                        notificationTitle.style.display = "none";

                        document.getElementById("notifyChecker").value = 0; 

                    }
                     
            }
        </script>
    </body>
</html>
<?PHP } ?>
