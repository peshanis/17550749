<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['rollid'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$learningPreperation=$_POST['learningPreperation'];
$classRoomManagement=$_POST['classRoomManagement'];
$deliveryInstructions=$_POST['deliveryInstructions'];
$followUps=$_POST['followUps'];
$responsibilities=$_POST['responsibilities']; 
$overallRate=$_POST['overallRate']; 
$lecturer=$_POST['lecturer'];  

$sql="INSERT INTO  tblevaluation(LearningPrperation,ClassRoomManagement,DeliveryInstructions,FollowUps,Responsibilities,OverallScore,LecturerId) VALUES(:learningPreperation,:classRoomManagement,:deliveryInstructions,:followUps,:responsibilities,:overallRate,:lecturer)";
$query = $dbh->prepare($sql);
$query->bindParam(':learningPreperation',$learningPreperation,PDO::PARAM_STR);
$query->bindParam(':classRoomManagement',$classRoomManagement,PDO::PARAM_STR);
$query->bindParam(':deliveryInstructions',$deliveryInstructions,PDO::PARAM_STR);
$query->bindParam(':overallRate',$overallRate,PDO::PARAM_STR);
$query->bindParam(':followUps',$followUps,PDO::PARAM_STR);
$query->bindParam(':responsibilities',$responsibilities,PDO::PARAM_STR);
$query->bindParam(':lecturer',$lecturer,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Evaluation added successfully";
}
else 
{
$error="Something went wrong. Please try again";
}

}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SMS Admin| Lecturer Evaluation </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script> 

   <style>
.glyphicon { 
    top: 9px; 
    color: goldenrod;
}

   </style>

    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Lecturer Evaluation</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                
                                        <li class="active">Lecturer Evaluation</li>
                                    </ul>
                                </div>
                             
                            </div> <br>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Fill the Lecturer Evaluation</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">
                                                <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Lecture</label>
                                                        <div class="col-sm-10">
 <select name="lecturer" class="form-control" id="lecturer" required="required">
<option value="">Select Lecturer</option>

<?php
// code Student Data
 
$rollid=$_SESSION['rollid'];
$classid=$_SESSION['classid'];
$qery = "SELECT   tblstudents.StudentId from tblstudents join tblclasses on tblclasses.id=tblstudents.ClassId where tblstudents.RollId=:rollid and tblstudents.ClassId=:classid ";
$stmt = $dbh->prepare($qery);
$stmt->bindParam(':rollid',$rollid,PDO::PARAM_STR);
$stmt->bindParam(':classid',$classid,PDO::PARAM_STR);
$stmt->execute();
$resultss=$stmt->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($stmt->rowCount() > 0)
{
foreach($resultss as $row)
{  $student_ID = $row->StudentId; } } ?> 
 


<?php $sql = "SELECT LecturerId,LecturerName,SubjectName,SubjectCode 
FROM srms.tbllecturers left join tblsubjects on tbllecturers.ModuleId = tblsubjects.id
left join tblsubjectcombination on tblsubjects.id=tblsubjectcombination.SubjectId
left join tblclasses on tblsubjectcombination.ClassId = tblclasses.id
left join tblstudents on tblclasses.id = tblstudents.ClassId
where tblstudents.StudentId = '+$student_ID+' ";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->LecturerId); ?>"><?php echo htmlentities($result->LecturerName); ?>--<?php echo htmlentities($result->SubjectName); ?> (<?php echo htmlentities($result->SubjectCode); ?>)&nbsp;<?php echo htmlentities($result->Section); ?></option>
<?php }} ?>
 </select>
                                                        </div>
                                                    </div>

                                                    

<div class="form-group">
<label for="default" class="col-sm-5 control-label">Planning and Preperation for learning</label>
<input type="number" class="rating" id="learningPreperation" name="learningPreperation" data-min="1" data-max="5" value="0">
</div>

<div class="form-group">
<label for="default" class="col-sm-5 control-label">Classroom Management</label>
<input type="number" class="rating" id="classRoomManagement" name="classRoomManagement" data-min="1" data-max="5" value="0">
</div>
 
<div class="form-group">
<label for="default" class="col-sm-5 control-label">Delivery of Instructions</label>
<input type="number" class="rating" id="deliveryInstructions" name="deliveryInstructions" data-min="1" data-max="5" value="0">
</div>

<div class="form-group">
<label for="default" class="col-sm-5 control-label">Monitoring, Assessment and Follow-Ups</label>
<input type="number" class="rating" id="followUps" name="followUps" data-min="1" data-max="5" value="0">
</div>

<div class="form-group">
<label for="default" class="col-sm-5 control-label">Professional Responsibilities</label>
<input type="number" class="rating" id="responsibilities" name="responsibilities" data-min="1" data-max="5" value="0">
</div>


<div class="form-group">
<label for="default" class="col-sm-5 control-label">Overall Rating</label>
<input type="number" class="rating" id="overallRate" name="overallRate" data-min="1" data-max="5" value="0">
</div>                                                   

                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-5 col-sm-11">
                                                            <button type="submit" name="submit" class="btn btn-primary">Add</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });

            (function ($) {

$.fn.rating = function () {

  var element;

  // A private function to highlight a star corresponding to a given value
  function _paintValue(ratingInput, value) {
    var selectedStar = $(ratingInput).find('[data-value=' + value + ']');
    selectedStar.removeClass('glyphicon-star-empty').addClass('glyphicon-star');
    selectedStar.prevAll('[data-value]').removeClass('glyphicon-star-empty').addClass('glyphicon-star');
    selectedStar.nextAll('[data-value]').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
  }

  // A private function to remove the selected rating
  function _clearValue(ratingInput) {
    var self = $(ratingInput);
    self.find('[data-value]').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
    self.find('.rating-clear').hide();
    self.find('input').val('').trigger('change');
  }

  // Iterate and transform all selected inputs
  for (element = this.length - 1; element >= 0; element--) {

    var el, i, ratingInputs,
      originalInput = $(this[element]),
      max = originalInput.data('max') || 5,
      min = originalInput.data('min') || 0,
      clearable = originalInput.data('clearable') || null,
      stars = '';

    // HTML element construction
    for (i = min; i <= max; i++) {
      // Create <max> empty stars
      stars += ['<span class="glyphicon glyphicon-star-empty" data-value="', i, '"></span>'].join('');
    }
    // Add a clear link if clearable option is set
    if (clearable) {
      stars += [
        ' <a class="rating-clear" style="display:none;" href="javascript:void">',
        '<span class="glyphicon glyphicon-remove"></span> ',
        clearable,
        '</a>'].join('');
    }

    el = [
      // Rating widget is wrapped inside a div
      '<div class="rating-input">',
      stars,
      // Value will be hold in a hidden input with same name and id than original input so the form will still work
      '<input type="hidden" name="',
      originalInput.attr('name'),
      '" value="',
      originalInput.val(),
      '" id="',
      originalInput.attr('id'),
      '" />',
      '</div>'].join('');

    // Replace original inputs HTML with the new one
    originalInput.replaceWith(el);

  }

  // Give live to the newly generated widgets
  $('.rating-input')
    // Highlight stars on hovering
    .on('mouseenter', '[data-value]', function () {
      var self = $(this);
      _paintValue(self.closest('.rating-input'), self.data('value'));
    })
    // View current value while mouse is out
    .on('mouseleave', '[data-value]', function () {
      var self = $(this);
      var val = self.siblings('input').val();
      if (val) {
        _paintValue(self.closest('.rating-input'), val);
      } else {
        _clearValue(self.closest('.rating-input'));
      }
    })
    // Set the selected value to the hidden field
    .on('click', '[data-value]', function (e) {
      var self = $(this);
      var val = self.data('value');
      self.siblings('input').val(val).trigger('change');
      self.siblings('.rating-clear').show();
      e.preventDefault();
      false
    })
    // Remove value on clear
    .on('click', '.rating-clear', function (e) {
      _clearValue($(this).closest('.rating-input'));
      e.preventDefault();
      false
    })
    // Initialize view with default value
    .each(function () {
      var val = $(this).find('input').val();
      if (val) {
        _paintValue(this, val);
        $(this).find('.rating-clear').show();
      }
    });

};

// Auto apply conversion of number fields with class 'rating' into rating-fields
$(function () {
  if ($('input.rating[type=number]').length > 0) {
    $('input.rating[type=number]').rating();
  }
});

}(jQuery));

        </script>
    </body>
</html>
<?PHP } ?>
